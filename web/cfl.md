---
layout: page
title: "Call for locations"
permalink: /cfl/
---

## The Call for Locations is Now Open!
We are currently looking for a location to host the 2019 Linux App Summit (LAS 2019).

LAS is a co-hosted event by [GNOME](https://www.gnome.org/) and [KDE](https://kde.org/) -- the two largest Linux desktop communities in the world. 
The summit brings together app designers, developers, and distributors from all over the world to expand the Linux application market. 

**Why help us host?**  
Hosting an internationally-attended summit like LAS is a great way to put your city on the map for free and open source enthusiasts, and the top experts in Linux application design and development. 
If you've been interested in contributing to GNOME, KDE, or Free and Open Source Software in general, this is a great way to do it. Events like this serve to accelerate change and spark innovative ideas.

The two previous summits have been held in Portland, Oregon; and Denver, Colorado; respectively. As the summit continues to expand this year, we are also open to international bids.

**How to submit a proposal**  
Interested parties are hereby invited to submit a proposal to the Linux App Summit Committee. The proposal must follow the [Bid Guidelines](http://linuxappsummit.org/bids/) and the event must adhere to the [LAS Event Code of Conduct](http://linuxappsummit.org/coc/). 

Please send an email with your statement of interest, and later your proposal, to appsummit@lists.freedesktop.org. 

*Relevant dates*
 * *15 May* - statement of interest due *(this just lets us know where conference locations may be and helps you establish communication with the committee)*
 * *5 June* - bid proposal due *(this should adhere to the Bid Guidelines mentioned above)*
 * *19 June* - location announcement

After you submit your bid proposal, we might invite you to present it in more details at one of our regular meetings, or send you additional questions and requests. If you have any questions, or need help, please email the committee.

We look forward to hearing from you soon!

