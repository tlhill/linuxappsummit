---
layout: page
permalink: /bids/
toc: false
---

# Bid guidelines

## Venue
We rely on organizers to help us draw in participants, so the venue should be big enough to fit the number of people you think will attend. Historically, this conference has drawn between 80 to 100 people and we hope to keep growing the conference each year.

Here are some things to keep in mind as you look for a venue, and as you think about the event:

* Accessiblity
* One main room for keynotes
* Exhibtor space
* Internet (wifi with fast connection for hacking preferred)
* Rooms for hacking / breakout rooms
* Child care
* Family rooms
* Gender neutral bathrooms
* Resonable security for registrants and attendees

## Travel and Transportation
The conference will draw people from out of town so make sure to list the different types of travel available to get to the venue (e.g. is it close to an international airport, is there public transportation available to get to the venue, etc?).
Accommodation

What kind of accommodation options are available? For example, are there many hotels to choose from, AirBnBs, dorms we can rent?

Is most accommodation within walking distance, or will people need to take transportation? Try to gather some sample rates so we can get an idea of the cost participants will expect to pay.

## Catering / Meals
Having food nearby is important in order to maximize the amount of time people can spend at the conference itself.

Here are some things to keep in mind:
* Is catering allowed? Organization may want to sponsor a breakfast or snacks
* Food restrictions. Are there options for people with food allergies and sensitivities? (e.g. vegan, gluten free, vegetarian, allergies)
* Coffee and tea. Are there places for people to get a quick caffeine break?

## Video Recording
What kind of video recording options are available? We’d like to record speakers and post videos online after the event.

## Social Events and Activities
Group activities and social events are an important part of creating a conference environment where people can make new friends and network. When thinking about group events, remember to be inclusive about the type of activities you propose.

Here are some things to consider:

* Accessibility
* Minors or children
* Food restrictions (vegan, gluten free, allergies)
* People who don’t drink alcohol

## Budget
Please create an expected budget for the conference. [Here is a sample budget](/stuff/budget-las2018.ods) from LAS 2018.

## Local Team
Who can help organize this event? There should be a main liaison and people who can help with things like planning, outreach, marketing, set up, take down, and volunteering during the event itself.

## Local Support
Are there groups or organizations in the area that can help you organize the event, or help with sponsorship?

## Sponsorship
Are there organizations in your area that can help sponsor the conference? There will be some fundraising done at a global level, but we expect organizers to also help with securing local sponsorship.
